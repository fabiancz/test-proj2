Proj2
=====

Requirements
------------

PHP 5.6 or higher.


Installation
------------

Make directories `temp/` and `log/` writable.
composer install


Web Server Setup
----------------

The simplest way to get started is to start the built-in PHP server in the root directory of your project:

	php -S localhost:8182 -t www

Then visit `http://localhost:8182` in your browser to see the welcome page.
