<?php

class MyTranslator implements Nette\Localization\ITranslator
{
	public function translate($message, $count = NULL)
    {
    	$lang = 'en'; // FIXME: nette way getting of $lang
        include __DIR__.'/../shared/translations/'.$lang.'.php';
		if (isset($messages[$message])) {
			return $messages[$message];
		}
    }
}