<?php

namespace App\Presenters;

use Nette;


class HomepagePresenter extends Nette\Application\UI\Presenter
{
	private $translator;

	public function startup()
	{
		parent::startup();

		$this->translator = new \MyTranslator;
	}

	public function renderDefault()
	{
		$cpEmail = new \Shared\CPEmail($this->translator);
		$cpEmail->sendMail('tomas.babicky@slayoutmaschine.de', 'Test mail from Nette');

		$this->template->setTranslator($this->translator);

		$utils = new \Shared\Utils;
		$this->template->utilsFist = $utils->first();
	}
  
  public function Bla()
  {
    return 1;
  }
}
